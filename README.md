# yrjw.ORM.Chimp

#### 介绍
It is not the encapsulation of ORM，a based on EF + dapper + Autofac, is repository and unitofwork


#### 安装教程

1.  程序包管理器控制台，使用 NuGet命令安装。
PM> Install-Package yrjw.ORM.Chimp

2.  或者直接在项目文件.csproj中引入包
<PackageReference Include="yrjw.ORM.Chimp" Version="1.0.0" />

#### 使用说明

1.  创建实体对象，继承IEntity

```
    /// <summary>
    /// 学生信息表
    /// </summary>
    [Table("StudentInfo")]
    public class StudentInfo: IEntity
    {
        [Key]
        public virtual int Id { get; set; }
        /// <summary>
        /// 学生姓名
        /// </summary>
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 民族
        /// </summary>
        public int NationId { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
    }
```

2.  创建上下文，继承BaseDbContext，使用base.OnModelCreating()，无需添加DbSet<T>

```
    public class myDbContext: BaseDbContext
    {
        public myDbContext()
        {
        }

        public myDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed(); //种子数据
        }
    }
```

3.  添加扩展方法，初始化种子数据。

```
public static class ModelBuilderExtensions
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="modelBuilder"></param>
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentInfo>().HasData(
                new StudentInfo { Id = 1, Name = "张三", Sex = 1, NationId = 1, Phone="13902451189"}
                );
        }
    }
```
4.  创建仓储接口和实现类，service继承IDependency接口，可支持通过属性依赖注入方式（使用Autofac依赖注入）。

```
    public interface IStudentInfoService
    {
        IUnitOfWork UnitOfWork { get; }

        Task<IResultModel> QueryList();
    }
```

```
    public class StudentInfoService: IStudentInfoService, IDependency
    {
        private readonly Lazy<IMapper> _mapper;
        private readonly Lazy<IRepository<StudentInfo>> repStudentInfo;

        public IUnitOfWork UnitOfWork { get; }

        public StudentInfoService(Lazy<IMapper> mapper, IUnitOfWork unitOfWork, Lazy<IRepository<StudentInfo>> repStudentInfo)
        {
            this._mapper = mapper;
            this.UnitOfWork = unitOfWork;
            this.repStudentInfo = repStudentInfo;
        }

        public async Task<IResultModel> QueryList()
        {
            var list = await repStudentInfo.Value.TableNoTracking.ProjectTo<StudentInfoDTO>(_mapper.Value.ConfigurationProvider).ToListAsync();
            return ResultModel.Success<IList<StudentInfoDTO>>(list);
        }
    }
```
5.  添加控制器Controller

```
    [Description("学生信息")]
    [Route("api/[controller]/[action]")]
    public class StudentInfoController : ControllerBase
    {
        private readonly ILogger<StudentInfoController> _logger;

        public Lazy<IStudentInfoService> StudentInfoService { get; set; }

        public StudentInfoController(ILogger<StudentInfoController> logger)
        {
            _logger = logger;
        }

        [Description("获取学生列表")]
        [ResponseCache(Duration = 0)]
        [HttpGet]
        public Task<IResultModel> QueryList()
        {
            return StudentInfoService.Value.QueryList();
        }
    }
```
6.  最关键一步，Startup.cs中注入服务，setting.AssemblyName为当前运行的api程序集命名空间。

```
    public virtual void ConfigureServices(IServiceCollection services)
    {
        if (setting.DbType == yrjw.ORM.Chimp.DbType.MYSQL)
        {
            services.AddChimp<myDbContext>(opt => opt.UseMySql(setting.ConnectionString,
                b => b.MigrationsAssembly(setting.AssemblyName)));
        }
        else
        {
            services.AddChimp<myDbContext>(opt => opt.UseSqlServer(setting.ConnectionString,
                b => b.MigrationsAssembly(setting.AssemblyName)));
        }
    }
```

#### 详细使用说明

[使用说明](https://github.com/longxianghui/chimp)

#### 关于Chimp

在Leo.Chimp包基础上添加了Autofac依赖注入，封装返回接口模型IResultModel，支持.net core 3.1版本上使用。
